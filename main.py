
def delete_file():
    file_path='file.txt'
    try:
        os.remove(file_path)
    except:
        print(''The system cannot find the the file specifed'')
        return False


# Здесь был добавлен комментарий и код от Дмитрия Красикова 1231 ака User2 

def read_txt_file():
	file = "file.txt"
	with open(file, "r", encoding="utf-8") as f:
		data = f.read()
		pring(data)
		return data

# User1 : пишет в файл
def write_in_txt_file():
    with open('file.txt', 'w') as writer:
        writer.write('I love Git')
        return True

if __name__ == "__main__":
    if write_in_txt_file():  # создаём и пишем в файл "I love Git"
        print("[OK] write")
    
    print(read_txt_file())  # читаем содержимое
    
    if delete_file():   # удаляем файл
        print("[OK] delete")

